var database = require('../config/database.js');
var conString = database.conString;

var Sequelize = require('sequelize');
var sequelize = new Sequelize(conString);

/**
 *
 * Initialize the connection when the server starts up.
 * This is done here just to make the call once as the documentations suggests.
 *
 * http://docs.sequelizejs.com/manual/installation/getting-started.html#setting-up-a-connection
 *
 */
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = {
  // connect: function() {
  //   sequelize
  //     .authenticate()
  //     .then(() => {
  //       console.log('Connection has been established successfully.');
  //     })
  //     .catch(err => {
  //       console.error('Unable to connect to the database:', err);
  //     });
  // },

  login: function(_req, res) {
    console.log('here: login');

    /**
     *
     * Here a query to the database should be done to validate the user:
     * Sequelize should be called quering for the user with the email from the params.
     *
     */

     // This is a generic response just so the flow can continue.
    return res.json({});
  },

  signUp: function(_req, res) {
    console.log('here: signUp');

    /**
     *
     * Here a query to the database should be done to create the user:
     * Sequelize should be called pushing a new row to the database on the users table.
     *
     */

     // This is a generic response just so the flow can continue.
    return res.json({});
  }
}
